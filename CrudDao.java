package pl.bookmakers.strategieschecker.example;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

abstract class CrudDao<ENTITY> {

    private final CrudRepository<ENTITY, Long> repository;

     CrudDao(CrudRepository<ENTITY, Long> repository){
        this.repository = repository;
    }

    public <S extends ENTITY> S save(S entity) {
        return repository.save(entity);
    }

    public Iterable<ENTITY> findAll() {
        return repository.findAll();
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Optional<ENTITY> findById(Long id) {
        return repository.findById(id);
    }
}
