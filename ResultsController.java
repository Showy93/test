package pl.bookmakers.strategieschecker.controller;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.bookmakers.strategieschecker.data.enums.CourseFilterTypeEnum;
import pl.bookmakers.strategieschecker.data.enums.FilterEnum;
import pl.bookmakers.strategieschecker.data.enums.LeagueEnum;
import pl.bookmakers.strategieschecker.data.enums.SeasonEnum;
import pl.bookmakers.strategieschecker.data.enums.StrategyEnum;
import pl.bookmakers.strategieschecker.service.ResultsService;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

@Controller
@RequestMapping("/")
public class ResultsController {

    private final ResultsService resultsService;

    @Autowired
    ResultsController(ResultsService resultsService) {
        this.resultsService = resultsService;
    }

    @GetMapping("/results")
    private String getResults(Model model) {
        model.addAttribute("results", resultsService.generateResults());
        return "results";
    }

    @GetMapping("/history")
    private String getHistory(Model model,
                              @RequestParam SeasonEnum season,
                              @RequestParam LeagueEnum league,
                              @RequestParam StrategyEnum strategy,
                              @RequestParam FilterEnum filter,
                              @RequestParam String team,
                              @RequestParam CourseFilterTypeEnum courseFilter,
                              @RequestParam BigDecimal course) {
        model.addAttribute("history", resultsService.generateHistory(season, league, strategy, filter, team, courseFilter, course));
        return "history";
    }

    @GetMapping("/form")
    private String form() {
        return "form2";
    }

    @PostMapping(value = "/form" )
    @ResponseBody
    public byte[] handleFileUpload(InputStream inputStream) throws IOException {
        return IOUtils.toByteArray(inputStream);
    }
}
