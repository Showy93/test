package pl.bookmakers.strategieschecker.example;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SimpleCrudRepository<T> extends CrudRepository<Entity, Long> {
    <S extends Entity> S save(S var1);

    Iterable<Entity> findAll();

    void deleteById(Integer var1);

    Optional<Entity> findById(Integer var1);
}

