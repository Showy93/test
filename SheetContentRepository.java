package com.roche.giraffe.drive.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Arrays;

@Slf4j
@RequiredArgsConstructor
public class SheetContentRepository implements SheetRepository {

	public SheetContentRepository(String credentialJsonFileName, String applicationName) throws GeneralSecurityException, IOException {
		NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JacksonFactory jsonFactory = JacksonFactory.getDefaultInstance();
		GoogleCredential credential = GoogleCredential.fromStream(Files.newInputStream(Paths.get(credentialJsonFileName)))
				.createScoped(Arrays.asList(SheetsScopes.SPREADSHEETS ,DriveScopes.DRIVE_FILE, DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE));
		Drive drive = new Drive.Builder(httpTransport, jsonFactory, credential).setApplicationName(applicationName).build();
		Sheets sheets = new Sheets.Builder(httpTransport, jsonFactory, credential).setApplicationName(applicationName).build();
		Spreadsheet spreadsheet = new Spreadsheet()
				.setProperties(new SpreadsheetProperties()
						.setTitle(String.valueOf(System.currentTimeMillis())));
		spreadsheet = sheets.spreadsheets().create(spreadsheet)
				.setFields("spreadsheetId")
				.execute();
		drive.permissions().create(spreadsheet.getSpreadsheetId(),new Permission()
				.setRole("owner")
				.setType("user")
				.setEmailAddress("testowymarcin93@gmail.com"))
				.setTransferOwnership(true)
				.execute();
				//.setId("14220225847133018481")).execute();
//		File file = drive.files().get(spreadsheet.getSpreadsheetId()).execute();
//		Drive.Files.Update update = drive.files().update(file.getId(),file);
//		Drive.Files.Update updated = update.setAddParents("1YSsH7RXZl6rOg4y5a9ZQjXMEzF4T2sgi");
//		updated.execute();
		log.error("Spreadsheet ID: " + spreadsheet.getSpreadsheetId());

	}
}